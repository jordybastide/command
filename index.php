<?php

require ('autoload.php');

use App\Models\Radio;
use App\Models\Console;
use App\Models\ConsoleCommand;
use App\Models\RadioCommand;
use App\Models\RemoteControle;

$radio = new Radio();
$radioRemoteOn = new RadioCommand($radio);
$radioRemoteOff = new RadioCommand($radio);
$radioRemote = new RemoteControle($radioRemoteOn, $radioRemoteOff);

$console = new Console();
$consoleRemoteOn = new ConsoleCommand($console);
$consoleRemoteOff = new ConsoleCommand($console);
$consoleRemote = new RemoteControle($consoleRemoteOn,$consoleRemoteOff);

echo $radioRemote->pressButton1();
echo '<br>';
echo $radioRemote->pressButton2();
echo '<br>';
echo $consoleRemote->pressButton1();
echo '<br>';
echo $consoleRemote->pressButton2();
echo '<br>';
