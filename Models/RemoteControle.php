<?php

namespace App\Models;

use App\Interfaces\RemoteControleInterface;

class RemoteControle
{
    private $button1;
    private $button2;

    public function __construct(RemoteControleInterface $button1, RemoteControleInterface $button2)
    {
       $this->button1 = $button1;
       $this->button2 = $button2;
    }

    public function pressButton1()
    {
        $this->button1->execute();
    }

    public function pressButton2()
    {
        $this->button2->undo();
    }
}
