<?php

namespace App\Models;

use App\Models\Radio;
use App\Interfaces\RemoteControleInterface;

class RadioCommand implements RemoteControleInterface
{
    private $radio;

    public function __construct( Radio $radio)
    {
        $this->radio = $radio;
    }
    public function execute()
    {
        $this->radio->on();
    }
    public function undo()
    {
        $this->radio->off();
    }
}
