<?php

namespace App\Models;

use App\Models\Console;
use App\Interfaces\RemoteControleInterface;

class ConsoleCommand implements RemoteControleInterface
{
     private  $console;

     public function __construct(Console $console)
     {
         $this->console = $console;
     }
    public function execute()
    {
        $this->console->on();
    }
    public function undo()
    {
        $this->console->off();
    }
}
