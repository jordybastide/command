<?php

namespace App\Interfaces;

interface RemoteControleInterface
{
    public function execute();
    public function undo();
}
